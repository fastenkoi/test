package org.example;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.example.models.Cart;
import org.example.models.Product;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import java.util.List;

@Controller
public class CartController {
    @Resource
    private CalculationService calculationService;

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public String calculate(@RequestParam(value = "products") String products,
                            @RequestParam(value = "deliveryType") String deliveryType, Model model) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            List<Product> productsList = mapper.readValue(products, new TypeReference<List<Product>>() {});

            Cart cart = new Cart(productsList);
            calculationService.calculateProductsCost(cart, deliveryType);
            calculationService.calculateDeliveryCost(cart, deliveryType);
            calculationService.calculateDiscount(cart, deliveryType);

            double totals = calculationService.productsCost + calculationService.deliveryCost - calculationService.discount;
            String response = "<br/>Стоимость продуктов = " + calculationService.productsCost +
                    "<br/>Стоимость доставки = " + calculationService.deliveryCost +
                    "<br/>Скидка = " + calculationService.discount +
                    "<br/>ИТОГО = " + totals;

            model.addAttribute("response", response);
        } catch (JsonProcessingException e) {
            model.addAttribute("response", "ERROR!");
        }
        return "index";
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index(Model model) {
        model.addAttribute("response", "");
        return "index";
    }
}

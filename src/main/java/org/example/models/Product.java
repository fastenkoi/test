package org.example.models;

public class Product {
    private double cost;
    private int quantity;

    public Product() {
    }

    public Product(double cost, int quantity) {
        this.cost = cost;
        this.quantity = quantity;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}

package org.example.models;

import java.io.Serializable;
import java.util.List;

public class Cart implements Serializable {
    private List<Product> products;

    public Cart() {
    }

    public Cart(List<Product> products) {
        this.products = products;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }
}

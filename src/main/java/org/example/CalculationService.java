package org.example;

import org.example.models.Cart;
import org.springframework.stereotype.Component;

@Component(value = "calculationService")
public class CalculationService {
    double productsCost = 0.0;
    double deliveryCost = 0.0;
    double discount = 0.0;

    private double reactFactor = 1.05;

    void calculateProductsCost(Cart cart, String deliveryType) {
        if (deliveryType.equals("TOTIME")) {
            cart.getProducts().forEach(product -> productsCost += product.getCost() * product.getQuantity());
        } else if (deliveryType.equals("REACT")) {
            cart.getProducts().forEach(product -> productsCost += product.getCost() * product.getQuantity() * reactFactor);
        }
    }

    void calculateDeliveryCost(Cart cart, String deliveryType) {
        if (deliveryType.equals("TOTIME")) {
            deliveryCost = cart.getProducts().size() > 10 ? 100.0 : 300.0;
        } else if (deliveryType.equals("REACT")) {
            deliveryCost = productsCost > 1000.0 ? 0.0 : 500.0 * reactFactor;
        }
    }

    void calculateDiscount(Cart cart, String deliveryType) {
        if (deliveryType.equals("TOTIME")) {
            discount = productsCost >= 1000.0 && cart.getProducts().size() > 10 ? 100.0 : 0.0;
        } else if (deliveryType.equals("REACT")) {
            discount = productsCost >= 2000.0 ? 150.0 : 0.0;
        }
    }
}
